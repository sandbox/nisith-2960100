<?php

    /**
     * @file
     *
     * author: nisith
     */

    /**
     * List of site activities.
     */
    function _site_activities_list() {
        $site_activity_arr = db_select('site_activity_logs', 'sal')
                ->fields('sal')
                ->execute()
                ->fetchAll();
        $rows = array();
        foreach ($site_activity_arr as $site_activity_data)
        {
            $type = $site_activity_data->type;
            $user = $site_activity_data->user;
            $date_time = date('Y-m-d H:i:s', $site_activity_data->timestamp);
            $attribute_name = $site_activity_data->attribute_name;
            $description = $site_activity_data->short_desc;
            $more_details = l('View', 'admin/reports/site_activity/details/' . $site_activity_data->sa_id);
            $rows[] = array(
                $type,
                $attribute_name,
                $user,
                $date_time,
                $description,
                $more_details,
            );
        }
        $header = array('Type', 'Name', 'User', 'Date and Time', 'Description', 'More Details');
        $output = '';
        $output .= theme('table', array('header' => $header, 'rows' => $rows));
        return $output;
    }

    /**
     * List of site activities.
     */
    function _site_activities_details($sa_id) {
        if (is_numeric($sa_id))
        {
            $site_activity = db_select('site_activity_logs', 'sal')
                    ->fields('sal')
                    ->condition('sal.sa_id', $sa_id)
                    ->execute()
                    ->fetchObject();
            $data = unserialize($site_activity->description);
            $data_val = '';
            $value = '';
            foreach ($data as $indx => $val) {
                if (!is_array($val)){
                    $value = $val;
                }
                $data_val .= '<p><span><b>' . $indx . '</b> : </span><span>' . $value . '</span></p>';
            }
            $row = array();
            $row[0] = array(
              $data_val 
            );
           
            $header = array('CLI Details');
            $output = '';
            $output .= theme('table', array('header' => $header, 'rows' => $row));
            return $output;
        }
        else
        {
            drupal_set_message(t('Invalid activity'), 'warning');
        }
    }
    